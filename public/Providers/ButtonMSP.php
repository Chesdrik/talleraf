<?php
namespace Filloy\pmUIKitMaterial_v1;

use Form;
use Illuminate\Support\ServiceProvider;

class ButtonMSP extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

	  /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Submit button
        Form::macro('MDsubmit', function ($label, $id = '', $c_options = []) {
            $options = ['class' => 'btn btn-light btn--icon-text btn-puma-off pull-right', 'icon' => 'check'];
            $options = array_merge($options, $c_options);

            return '<div class="col-md-12">
                        <button class="'.$options['class'].'" id="'.$id.'" class="btn btn-light btn--icon-text btn-puma-off pull-right">
                            <i class="zmdi zmdi-'.$options['icon'].'"></i> '.$label.'
                        </button>
                    </div>';


            $btn = '<button type="submit" class="'.$class.'" id="'.$id.'">'.$label.'</button>';
            return $btn;
        });    
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
