<?php
namespace Filloy\pmUIKitMaterial_v1;

use Form;
use Illuminate\Support\ServiceProvider;

class FormMSP extends ServiceProvider
{
     /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Validation in each field
        // @if ($errors->has('name'))
        // <div class='col-md-12'>  <p class="error-p" ><small>{{ $errors->first('name') }}</small></p>
        // </div>
        // @endif

        //INPUTS

        // Input
        // for default id = $name and required = "true"
        // you can add id, classes and placeholder or any option input in $options example:
        // $options = [.... 'class' => '... my-class','id' => 'my-id', 'placeholder' => 'my-placeholder' ]
          Form::macro('MDtext', function ($title, $name, $value, $class = 'row', $options = []) {

              $input = '<div class="'.$class.'">
                          <label class="col-sm-2 col-form-label">'.$title.'</label>
                          <div class="col-sm-10">
                            <div class="form-group">
                                '.Form::text($name, $value,array_merge(['class' => 'form-control','id' => $name,'required' => 'true'],$options)).'
                                <i class="form-group__bar"></i>
                            </div>
                          </div>
                      </div>';

              return $input;
          });

        // Password
        // for default id = $name and required = "true"
        // you can add id, classes and placeholder or any option input in $options example:
        // $options = [.... 'class' => '... my-class','id' => 'my-id', 'placeholder' => 'my-placeholder' ]
        Form::macro('MDpassword', function ($title, $name, $class = 'row',$options = []) {
            $input = '<div class="'.$class.'">
                        <label class="col-sm-2 col-form-label">'.$title.'</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                '.Form::password($name,array_merge(['class' => 'form-control','id' => $name,'required' => 'true'],$options)).'
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>';

            return $input;
        });

        // Number
        // you can add id, classes , max , mint etc or any option input=number in $options example:
        // $options = [ 'max => 10, 'min' => 3]
        Form::macro('MDnumber', function ($title, $name, $value, $class = 'row',$options = []) {
            $input = '<div class="'.$class.'">
                        <label class="col-sm-2 col-form-label">'.$title.'</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                '.Form::number($name, $value,array_merge(['class' => 'form-control','id' => $name,'required' => 'true'],$options)).'
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>';

            return $input;
        });


        // Textarea
        // you can add id, classes and placeholder or any option  in $options example:
        // $options = [.... 'class' => '... my-class','id' => 'my-id', 'placeholder' => 'my-placeholder' ]
        Form::macro('MDtextarea', function ($title, $name, $value, $class = 'row', $options = []) {
            $input = '<div class="'.$class.'">
                        <label class="col-sm-2 col-form-label">'.$title.'</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                '.Form::textarea($name, $value,array_merge(['class' => 'form-control','id' => $name,'required' => 'true'],$options)).'
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>';

            return $input;
        });

        //-----------------------
        // Latlong
        Form::macro('MDlatlong', function ($title, $name, $value, $class = 'row') {
            $input = '<div class="'.$class.'">
                        <label class="col-sm-2 col-form-label">'.$title.'</label>
                        <div class="formcol-sm-10">
                            <div class="form-group">
                                <input class="form-control input-mask" data-mask="000.0000000" placeholder="000.0000000" autocomplete="off" type="text" name="'.$name.'" value="'.$value.'">
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>';

            return $input;
        });
        //----------------------


        // Select
        Form::macro('MDselect', function ($title, $name, $value, $options, $class = 'col-sm-12') {
            $option = '';
            foreach ($options as $key => $valor) {
              $option .= '<option value="'.$key.'">'.$valor.'</option>';
            }
            $input = '<div class="'.$class.'">
                          <div class="form-group col-sm-12">
                              <label class="col-md-3 control-label" for="'.$name.'">'.$title.'</label>
                              <div class="select col-md-9">
                                  <select class="form-control" id="'.$name.'" name="'.$name.'">'
                                    .$option.
                                  '</select>
                              </div>
                          </div>
                      </div>';
            return $input;
        });



        // Toggle
        Form::macro('MDtoggle', function ($title, $name, $checked, $checked_value = '', $class = 'row') {
            $input = '<div class="'.$class.'">
                        <label class="col-md-3 control-label" for="'.$name.'">'.$title.'</label>
                        <div class="select col-md-9">
                            '.Form::checkbox($name, $checked_value, $checked, ['class' => 'toggle-switch__checkbox']).'
                            <i class="toggle-switch__helper"></i>
                        </div>
                    </div>';
            return $input;
        });


      // Datepicker
      Form::macro('MDdate', function ($title, $name, $value, $class = 'row') {
          $input = '<div class="'.$class.'">
                      <label class="col-md-3 control-label" for="'.$name.'">'.$title.'</label>
                      <div class="input-group form-group">
                          <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                          <div class="dtp-container">
                              '.Form::text($name, $value, ['class' => 'form-control date-picker2']).'
                          </div>
                      </div>
                    </div>';

          return $input;
      });

      Form::macro('MDfile', function ($title, $name, $value,$accept, $class = 'col-sm-12', $c_options = ['class' => 'form-control form-control-rounded']) {
        $input = ' <div class="'.$class.'">
                    <div class="form-group">
                        <label for="'.$name.'">'.$title.'</label>
                        <input type="file" id="'.$name.'" name="'.$name.'" accept="'.$accept.'">
                    </div>
                </div>';

            return $input;
      });



    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
