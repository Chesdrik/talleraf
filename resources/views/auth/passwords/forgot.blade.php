@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif

<div class="login__block" id="l-forget-password">
    <div class="login__block__header palette-Purple bg">
        <i class="zmdi zmdi-account-circle"></i>
        ¿Olvidaste tu contraseña?

        <div class="actions actions--inverse login__block__actions">
            <div class="dropdown">
                <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>

                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-login" href="">¿Ya tienes una cuenta?</a>
                    <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-register" href="">Crear una cuenta</a>
                </div>
            </div>
        </div>
    </div>

    <div class="login__block__body">
        <p class="mt-4">Te enviaremos un correo para la recuperación de tus datos.</p>
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="form-group form-group--float form-group--centered">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <label>Correo</label>
                <i class="form-group__bar"></i>
            </div>
            <button type="submit" href="index.html" class="btn btn--icon login__block__btn"><i class="zmdi zmdi-long-arrow-right"></i></button>
        </form>
    </div>
</div>
