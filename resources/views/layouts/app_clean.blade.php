<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="{{ env('APP_NAME') }}">
		<meta name="author" content="">
		<meta name="keyword" content="{{ env('APP_NAME') }}">
		<link rel="shortcut icon" href="img/favicon.png">

		<title>{{ env('APP_NAME') }}</title>

		@include('layouts.includes.head_includes')
	</head>
	<body data-ma-theme="blue-grey">
		@yield('content')
		@include('layouts.includes.footer_includes')
		@yield('after_includes')
	</body>
</html>
