<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{ env('APP_NAME') }}">
    <meta name="author" content="">
		<meta name="keyword" content="{{ env('APP_NAME') }}">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="shortcut icon" href="img/favicon.png">
		<!-- CSRF dToken -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>{{ env('APP_NAME') }}</title>
		@include('layouts.includes.head_includes')
	</head>
	<body data-ma-theme="blue-grey">
		<main class="main">
            <div class="page-loader">
                <div class="page-loader__spinner">
                    <svg viewBox="25 25 50 50">
                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>
		    @include('layouts.includes.header')
			    @yield('breadcrum')
			@include('layouts.includes.sidebar')
			<section class="content">
				@yield('content')
			</section>
		</main>
			@include('layouts.includes.footer')
			@include('layouts.includes.footer_includes')
			@yield('layouts.after_includes')
	</body>
</html>
