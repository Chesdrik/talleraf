<!-- Vendor CSS -->
<link href="{{ URL::asset('assets/material_admin/vendors/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/material_admin/vendors/material-design-iconic-font/css/material-design-iconic-font.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/material_admin/vendors/animate.css/animate.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/material_admin/vendors/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/material_admin/vendors/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/material_admin/vendors/lightgallery/css/lightgallery.min.css') }}" rel="stylesheet">


<!-- Font Awesome -->
<link href="{{ URL::asset('assets/material_admin/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">


<!-- Datetime
<link rel="stylesheet" href="{{ URL::asset('/assets/material_admin/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
 -->
<!-- DataTables 
<link href="{{ URL::asset('assets/material_admin/template/vendors/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet">
-->
<!-- Select 
<link href="{{ URL::asset('assets/material_admin/vendors/bootstrap-select/dist/css/bootstrap-select.css') }}" rel="stylesheet">
-->

<!-- CSS -->
<link href="{{ URL::asset('assets/material_admin/css/app.min.css') }}" rel="stylesheet">
