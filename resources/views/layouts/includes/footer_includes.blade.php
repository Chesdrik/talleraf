<!-- Javascript Libraries -->
<script src="{{ URL::asset('assets/material_admin/vendors/jquery/jquery.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/popper.js/popper.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/jquery-scrollLock/jquery-scrollLock.min.js') }}"></script>


<!-- Flot Chart Libraries -->
<script src="{{ URL::asset('assets/material_admin/vendors/flot/jquery.flot.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/flot.curvedlines/curvedLines.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/jqvmap/maps/jquery.vmap.world.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/salvattore/salvattore.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/sparkline/jquery.sparkline.min.js') }}"></script>

<script src="{{ URL::asset('assets/material_admin/vendors/flot/jquery.flot.time.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/flot/jquery.flot.pie.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/flot.orderbars/jquery.flot.orderBars.js') }}"></script>




<script src="{{ URL::asset('assets/material_admin/vendors/moment/moment.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/fullcalendar/fullcalendar.min.js ') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/simpleWeather/jquery.simpleWeather.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/Waves/dist/waves.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/bootstrap-growl/bootstrap-growl.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/lightgallery/js/lightgallery-all.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ URL::asset('assets/material_admin/vendors/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/datatables-buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/datatables-buttons/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/jszip/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/material_admin/vendors/datatables-buttons/buttons.html5.min.js') }}"></script>




<!-- Select -->
<script src="{{ URL::asset('assets/material_admin/vendors/bootstrap-select/dist/js/bootstrap-select.js') }}"></script>


<!-- Placeholder for IE9 -->
<!--[if IE 9 ]>
    <script src="{{ URL::asset('assets/material_admin/vendors/jquery-placeholder/jquery.placeholder.min.js') }}"></script>
<![endif]-->

<script src="{{ URL::asset('assets/material_admin/js/app.min.js') }}"> </script>


