@if(!empty($breadcrumb))
    <!-- Breadcrumb -->
    <div id="page-head">
        <ol class="breadcrumb">
            @foreach($breadcrumb as $i => $b)
                <li class=" @if($i == count($breadcrumb)-1) active @endif">
                    @if(empty($b['url']))
                        <i class="ion-{{ $b['icon'] }}"></i> {!! $b['label'] !!}
                    @else
                        <a href="{{ $b['url'] }}" target="_self">
                            <i class="ion-{{ $b['icon'] }}"></i> {!! $b['label'] !!}
                        </a>
                    @endif
                </li>
            @endforeach
        </ol>
    </div>
@endif
