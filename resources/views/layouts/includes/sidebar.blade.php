<?php
$menu = array();
$menu[] = array('target' => '_self', 'route' => 'dashboard', 'url' => '/',  'icon' => 'home', 'text' => 'Dashboard');
$menu[] = array('target' => '_self', 'url' => '/logout',  'icon' => 'flag', 'text' => 'example');
// Logout
$menu[] = array('target' => '_self', 'url' => '/logout',  'icon' => 'mail-reply', 'text' => 'Cerrar sesión');
?>

 <aside class="sidebar">
    <div class="scrollbar-inner">
        <div class="user">
            <div class="user__info" data-toggle="dropdown">
                <img class="user__img" src="demo/img/profile-pics/8.jpg" alt="">
                <div>
                    <div class="user__name">Malinda Hollaway</div>
                    <div class="user__email">malinda-h@gmail.com</div>
                </div>
            </div>

            <div class="dropdown-menu">
                <a class="dropdown-item" href="">Ver perfil</a>
                <a class="dropdown-item" href="">Cerrar sesión</a>
            </div>
        </div>

        <ul class="navigation">
            @foreach($menu as $m)
                @if($m['url'] == Route::currentRouteName())
                    <li class="active">
                @else
                    <li class="">
                @endif
                    <a href="{{ url($m['url']) }}" target="{{ $m['target'] }}"><i class="fa fa-{{ $m['icon'] }}"></i> {{ $m['text'] }}</a>
                </li>
            @endforeach
        </ul>
    </div>
</aside>
