@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Veritifa tu e-mail') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Un nuevo link ha sido enviado a tu e-mail.') }}
                        </div>
                    @endif

                    {{ __('Antes de continuar, revisa tu correo electrónico con un link.') }}
                    {{ __('Si no recibiste el correo, ') }}, <a href="{{ route('verification.resend') }}">{{ __('click aquí para solicitar otro.') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
