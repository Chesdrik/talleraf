<div class="login__block" id="l-register">
    <div class="login__block__header palette-Blue bg">
        <i class="zmdi zmdi-account-circle"></i>
        Crear una cuenta

        <div class="actions actions--inverse login__block__actions">
            <div class="dropdown">
                <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>

                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-login" href="">¿Ya tienes una cuenta?</a>
                    <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-forget-password" href="">¿Olvidaste tu contraseña?</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="login__block__body">
            <div class="form-group form-group--float form-group--centered">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <label>Nombre</label>
                <i class="form-group__bar"></i>
            </div>

            <div class="form-group form-group--float form-group--centered">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <label>Correo</label>
                <i class="form-group__bar"></i>
            </div>

            <div class="form-group form-group--float form-group--centered">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <label>Contraseña</label>
                <i class="form-group__bar"></i>
            </div>

            <div class="form-group form-group--float form-group--centered">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

                <label>Confirmar contraseña</label>
                <i class="form-group__bar"></i>
            </div>

            <button type="submit" class="btn btn--icon login__block__btn"><i class="zmdi zmdi-check"></i></button>
        </div>
    </form>
</div>
