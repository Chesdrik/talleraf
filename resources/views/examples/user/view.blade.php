@extends('layouts.app')
@section('content')
<div id="page-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title"> Usuario {{ $usuario->name }} </h3>
				</div>
				<div class="panel-body">
					{!! Form::open(array('url'=>'/usuarios/'.$usuario->id, 'method'=>'PUT')) !!}
						@method('PUT')
						{!! Form::MDtext('Nombre:', 'name', $usuario->name) !!}
						@if($errors->has('name'))
							<div class="col-md-12">
								<p class="error-p"><small>{{ $errors->first('name') }}</small></p>
							</div>
							@endif

						{!! Form::MDtext('Email', 'email', $usuario->email, 'col-sm-12') !!}
						@if ($errors->has('email'))
							<div class="col-md-12">
								<p class="error-p"><small>{{ $errors->first('email') }} </small> </p>
							</div>
						@endif

						<div class="form-group cold-sm-12">
							<div class="col-md-12">
								<p class="col-md-3 control-label" >Selecciona una rol</p>
								<div class="form-group col-md-9">
									<select name="rol" id="rol" class="form-control" >
										<optgroup label="Rol"></optgroup>
										<option value="admin"> Administrador </option>
										<option value="user"> Usuario </option>
										<option value="school"> Eescuela </option>					
									</select>
								</div>
							</div>
						</div>


					{{ Form::token() }}
					{!! Form::MDsubmit('Editar', 'btn btn-primary pull-right') !!}
					{!! Form::close() !!}							
				</div>
			</div>
		</div>
	</div>
</div>
@endsection