@extends('layouts.app')
@section('content')
<div id="page-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Nuevo usuario</h3>
				</div>
				<div class="panel-body">
					{!! Form::open(array('url' => '/usuarios', 'method' => 'post' )) !!}

						{!! Form::MDtext('Nombre', 'name', "") !!}

						@if($errors->has('name'))
						<div class="cold-md-12"><p class="error-p"><small>{!! $error->first('name') !!}</small></p></div>
						@endif
						{!! Form::MDemail('Email', 'email', "") !!}

						@if($errors->has('email'))
						<div class="cold-md-12"><p class="error-p"><small>{!! $error->first('email') !!}</small></p></div>
						@endif

						{!! Form::MDpassword('Contraseña', 'password', "") !!}
						@if($errors->has('password'))
						<div class="cold-md-12"><p class="error-p"><small> {!! $error->first('password') !!} </small> </p> </div>
						@endif


						<div class="form-grup cold-sm-9">
							<div class="col-md-12">
								<p class="col-md-3 control-label" >Selecciona una rol</p>
								<div class="col-md-9">
									<select name="rol" id="rol" class="form-control" dir="rol">
										<optgroup label="Rol"></optgroup>
										<option value="admin"> Administrador </option>
										<option value="user"> Usuario </option>
										<option value="school"> Eescuela </option>					
									</select>
								</div>
							</div>
						</div>


					{{ Form::token() }}
					{!! Form::MDsubmit('Guardar', 'btn btn-primary pull-right') !!}
					{!! Form::close() !!}	
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

