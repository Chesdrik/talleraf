<div class="modal fade" id="change_status-{{$reservation->id}}" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(array('url' => '/reservaciones/'.$reservation->id, 'mehtod' => 'post')) !!}
                    @method('PUT')
                <!--Modal header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title">Cambio de status de la reservación</h4>
                </div>

                <!--Modal body-->
                <div class="modal-body">
                    <div class="row">
                      
                        <p class="col-md-3 control-label" >Selecciona un estatus</p>
                                <div class="col-md-9">
                                    
                                    <select name="status" id="status" class="form-control" dir="status">
                                        <optgroup label="status"></optgroup>
                                        <option value="pending"> Pendiente </option>
                                        <option value="cancelled"> Cancelada </option>
                                        <option value="finished"> Concluida </option>                  
                                    </select>
                                </div>
                    </div>
                </div>

                <!--Modal footer-->
                <div class="modal-footer">
                    {!! Form::MDsubmit('Guardar', 'btn btn-primary') !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>